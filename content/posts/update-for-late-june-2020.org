+++
title = "Update for late June 2020"
date = "2020-06-28"
+++
#+HUGO_BASE_DIR: ../
#+HUGO_SECTION: ./
#+HUGO_WEIGHT: auto
#+HUGO_AUTO_SET_LASTMOD: t
#+html: <br/>
#+AUTHOR: Caleb Woodbine <calebwoodbine.public@gmail.com>
#+DATE: 14th of December 2019
#+DATE_CREATED: 2019-06-28
#+DATE_UPDATED: 2019-06-28

#+html: <img style='margin-left: auto; margin-right: auto;' alt='flattrack shopping list desktop' src='/images/2020/06/flattrack-shopping-list-desktop.png'>
#+html: <br/>

Hey and welcome to a progress update for FlatTrack! Thank you for stopping by to check this update out. We're so excited to share what we've been busy working on.
FlatTrack has been under heavy development and has seen many changes and features implemented which we're super excited to share.

** Philosophy
FlatTrack aims to be a framework for flats and those who live together to set up a flow of common things in living situations - enabling for closer collaboration for the humans who live together. Being Free and Open Source, the tools that people use to manage home life can be collaboratively standardized - this means for a flexible and unopinionated experience.

** Shopping List
Make shopping lists.
Add items to lists.Group items by using tags. Add pricing and quantity to calculate what the expected price will be. Join your flatmates in adding items simultaneously.
Build your own flow for how you and your flat does shopping.

#+html: <img style='margin-left: auto; margin-right: auto;' alt='flattrack shopping list mobile' src='/images/2020/06/flattrack-shopping-list-mobile.png' width=350>
#+html: <br/>

*** Features
- group items using tags
- calculate list-wide pricing
- calculate tag-wide pricing
- sort items by their various properties
- search through items
- filter items by obtained, unobtained, or all
- real-time collaboration
- rename tags
and many more.
** Flatmates
View contact details, groups, and birthdays.

#+html: <img style='margin-left: auto; margin-right: auto;' alt='flattrack flatmates' src='/images/2020/06/flattrack-flatmates.png' width=350>
#+html: <br/>

Flatmembers, who are general users, can view information about all flatmembers (where provided).
** Flatmates (Admin)
Manage the user accounts of flatmembers as an admin. Create new user accounts. Complete sign up with a QR code or link. Manage group access.

#+html: <img style='margin-left: auto; margin-right: auto;' alt='flattrack flatmates' src='/images/2020/06/flattrack-admin-flatmates.png' width=350>
#+html: <br/>

Different groups provide various levels of access in FlatTrack.All users are flatmembers but only some are admins.Admin access allows control over all settings and fields, so it should be given out sparingly to only those are generally trusted.
** Wrap up
FlatTrack has had a great deal of progress recently. It's currently been under heavy use by some groups of people, so far there have been great results.Although there's still plenty to do, the project will be released as a stable beta in the coming months and FlatTrack's GitLab project will be publicly available.We're super excited for folks to use and contribute to the project. We're certain that there's a world of benefit awaiting.Until next time, thank you for checking in!
